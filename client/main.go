package main

import (
	"fmt"
	"flag"
	"encoding/json"

	"gitlab.com/nshibalov/golang-test-rest/client/app"
	"gitlab.com/nshibalov/golang-test-rest/client/app/api"
	"gitlab.com/nshibalov/golang-test-rest/server/app/models"
)


func cli(core *app.Core, search string) {
	defer fmt.Println()

	user := models.User{}
	if err := json.Unmarshal([]byte(search), &user); err != nil {
		fmt.Println("Error parsing User: " + err.Error())
		return
	}

	if found, duplicated, err := api.DuplicateUsers(core.DB, user); err == nil {
		fmt.Printf("Duplicated %d/%d Users\n", duplicated, found)
	} else {
		fmt.Println(err)
	}
}


func service(core *app.Core) {
	core.Run()
}


func main() {
	var is_service bool
	flag.BoolVar(&is_service, "service", false, "run as service")

	var search string
	flag.StringVar(&search, "search", `{"Duplicate": false}`, "a search string")

	flag.Parse()

	core := app.NewCore()
	defer core.Finalize()

	if is_service {
		service(core)
	} else {
		cli(core, search)
	}
}
