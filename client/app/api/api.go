package api

import (
	"fmt"
	"log"
	"net/url"

	"golang.org/x/net/context"

	"github.com/go-redis/redis"

	"gitlab.com/nshibalov/golang-test-rest/swagger/client"
	"gitlab.com/nshibalov/golang-test-rest/server/app/models"
)


func findUsers(
		db *redis.Client, user models.User) ([]models.User, error) {
	log.Println("Searching for:", user)
	return (&user).Find(db)
}


func prepareForDuplicate(user *models.User) {
		user.Id = nil

		is_duplicate := true
		user.Duplicate = &is_duplicate
}


func duplicateFallback(
		db *redis.Client, user_list []models.User) (int, error) {

	log.Println("Fallback to DB...")

	var counter int
	for _, user := range(user_list) {
		log.Println("Duplicating", user)

		if user.Duplicate != nil && *user.Duplicate {
			log.Println("User already Duplicate")
			continue
		}

		prepareForDuplicate(&user)

		if err := user.Create(db); err != nil {
			log.Println(err)
			continue
		}

		log.Println("Duplicated", user)
		counter += 1
	}

	return counter, nil
}


func duplicateAPI(
			db *redis.Client,
			user_list []models.User) (int, error) {

	confinguration := swagger.NewConfiguration()
	api_client := swagger.NewAPIClient(confinguration)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var counter int
	for _, user := range(user_list) {
		log.Println("Duplicating", user)

		if user.Duplicate != nil && *user.Duplicate {
			log.Println("User already Duplicate")
			continue
		}

		prepareForDuplicate(&user)

		new_user, _, err := api_client.UserApi.AddUser(ctx, user)
		if err != nil {
				switch err.(type) {
				case *url.Error:
					return duplicateFallback(db, user_list)
				default:
					log.Println(err)
					continue
				}
		}

		log.Println("Duplicated", new_user)
		counter += 1
	}

	return counter, nil
}


func DuplicateUsers(
		db *redis.Client, user models.User) (
			found int,
			duplicated int,
			err error) {

	if db == nil {
		err = fmt.Errorf("DB not initialized")
		return
	}

	if user_list, f_err := findUsers(db, user); f_err == nil {
		found = len(user_list)

		if found == 0 {
			err = fmt.Errorf("No Users found")
			return
		}

		duplicated, err = duplicateAPI(db, user_list)
		if err != nil {
			duplicated, err = duplicateFallback(db, user_list)
		}
	} else {
		err = f_err
	}

		return
}
