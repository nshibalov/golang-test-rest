package app

import (
	"github.com/gorilla/mux"
	"github.com/go-redis/redis"

	"gitlab.com/nshibalov/golang-test-rest/client/app/endpoints"
)


func NewRouter(db *redis.Client) *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/", endpoints.ClientCase(db)).Methods("GET", "POST")
	return router
}
