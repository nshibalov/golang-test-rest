package endpoints

import (
	"fmt"
	"net/http"
	"encoding/json"

	"github.com/go-redis/redis"

	"gitlab.com/nshibalov/golang-test-rest/client/app/api"
	"gitlab.com/nshibalov/golang-test-rest/server/app/models"
)


func userFromRequest(r *http.Request, user *models.User) error {
	if r.Body == nil {
		return fmt.Errorf("No User data")
	}

	if err := json.NewDecoder(r.Body).Decode(user); err != nil {
		return fmt.Errorf("Error parsing User: " + err.Error())
	}

	return nil
}


func ClientCase(db *redis.Client) http.HandlerFunc {
	return http.HandlerFunc(func (w http.ResponseWriter, r *http.Request) {

		user := models.User{}
		if err := userFromRequest(r, &user); err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		if found, duplicated, err := api.DuplicateUsers(db, user); err == nil {
			w.Write([]byte(fmt.Sprintf(
				"Duplicated %d/%d Users\n",
				duplicated,
				found)))
		} else {
			http.Error(w, "Error perfoming case: " + err.Error(), 400)
		}

	})
}
