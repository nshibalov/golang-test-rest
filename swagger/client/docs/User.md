# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int64** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Surname** | **string** |  | [optional] [default to null]
**Email** | **string** |  | [optional] [default to null]
**Age** | **int32** |  | [optional] [default to null]
**Duplicate** | **bool** |  | [optional] [default to null]
**Address** | [***Address**](Address.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


