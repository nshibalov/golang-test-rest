package endpoints

import (
	"fmt"
	"strconv"
	"net/http"
	"encoding/json"

	"github.com/gorilla/mux"
	"github.com/go-redis/redis"

	"gitlab.com/nshibalov/golang-test-rest/server/app/models"
)


func userFromRequest(r *http.Request, user *models.User) error {
	if r.Body == nil {
		return fmt.Errorf("No User data")
	}

	if err := json.NewDecoder(r.Body).Decode(user); err != nil {
		return fmt.Errorf("Error parsing User: " + err.Error())
	}

	return nil
}


func GetUser(db *redis.Client) http.HandlerFunc {
	return http.HandlerFunc(func (w http.ResponseWriter, r *http.Request) {
		user_id, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
		if err != nil {
			http.Error(w, "Wrong User ID format", 400)
			return
		}

		user := models.User{}
		user.Id = &user_id

		err = user.Get(db)
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		json.NewEncoder(w).Encode(user)
	})
}


func FindUser(db *redis.Client) http.HandlerFunc {
	return http.HandlerFunc(func (w http.ResponseWriter, r *http.Request) {

		user := models.User{}
		if err := userFromRequest(r, &user); err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		if user_list, err := user.Find(db); err == nil {
			json.NewEncoder(w).Encode(user_list)
		} else {
			http.Error(w, "Error finding Users: " + err.Error(), 400)
		}

	})
}


func CreateUser(db *redis.Client) http.HandlerFunc {
	return http.HandlerFunc(func (w http.ResponseWriter, r *http.Request) {

		user := models.User{}
		if err := userFromRequest(r, &user); err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		if err := user.Create(db); err != nil {
			http.Error(w, "Error creating User: " + err.Error(), 400)
			return
		}

		json.NewEncoder(w).Encode(user)

	})
}


func UpdateUser(db *redis.Client) http.HandlerFunc {
	return http.HandlerFunc(func (w http.ResponseWriter, r *http.Request) {

		user := models.User{}
		if err := userFromRequest(r, &user); err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		if err := user.Update(db); err != nil {
			http.Error(w, "Error updating User: " + err.Error(), 400)
			return
		}

		json.NewEncoder(w).Encode(user)

	})
}


func DeleteUser(db *redis.Client) http.HandlerFunc {
	return http.HandlerFunc(func (w http.ResponseWriter, r *http.Request) {

		user := models.User{}
		if err := userFromRequest(r, &user); err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		if err := user.Delete(db); err != nil {
			http.Error(w, "Error delete User: " + err.Error(), 400)
			return
		}

		json.NewEncoder(w).Encode(user)

	})
}
