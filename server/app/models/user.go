package models

import (
	"fmt"
	"strings"
	"strconv"

	"github.com/go-redis/redis"
)


const (
	UserClassStr = "User"
	IdKeyUser = "ID:User"

	IndexUser = "Index:User:"
	IndexContentUser = "Index:User:Content:"
	IndexUserClustered = "_Clustered"

	UserFieldName = "Name"
	UserFieldSurname = "Surname"
	UserFieldEmail = "Email"
	UserFieldAge = "Age"
	UserFieldDuplicate = "Duplicate"
	UserFieldAddressCity = "Address:City"
	UserFieldAddressStreet = "Address:Street"
)


type Address struct {
	City *string `json:"city,omitempty"`
	Street *string `json:"street,omitempty"`
}


type User struct {
	Id *int64 `json:"id,omitempty"`
	Name *string `json:"name,omitempty"`
	Surname *string `json:"surname,omitempty"`
	Email *string `json:"email,omitempty"`
	Age *int `json:"age,omitempty"`
	Duplicate *bool `json:"Duplicate,omitempty"`
	Address *Address `json:"address,omitempty"`
}


type Users []User
type FieldMap map[string]interface{}


func makeKey(class string, id int64) string {
	return fmt.Sprintf("%s:%d", class, id)
}


func (user User) String() string {
	var str_list []string
	for k, v := range user.ToFieldMap() {
		switch v.(type) {
		case int64:
			str_list = append(str_list, k + ": " + strconv.FormatInt(v.(int64), 10))
		case int:
			str_list = append(str_list, k + ": " + strconv.Itoa(v.(int)))
		case string:
			str_list = append(str_list, k + ": " + v.(string))
		case bool:
			str_list = append(str_list, k + ": " + strconv.FormatBool(v.(bool)))
		}
	}
	return fmt.Sprintf("User(%s)", strings.Join(str_list, "; "))
}


func (user *User) ToFieldMap() FieldMap {
	ret := make(FieldMap)

	if user.Name != nil {
		ret[UserFieldName] = *user.Name
	}

	if user.Surname != nil {
		ret[UserFieldSurname] = *user.Surname
	}

	if user.Email != nil {
		ret[UserFieldEmail] = *user.Email
	}

	if user.Age != nil {
		ret[UserFieldAge] = *user.Age
	}

	if user.Duplicate != nil {
		ret[UserFieldDuplicate] = *user.Duplicate
	}

	if user.Address != nil {
		if user.Address.City != nil {
			ret[UserFieldAddressCity] = *user.Address.City
		}

		if user.Address.Street != nil {
			ret[UserFieldAddressStreet] = *user.Address.Street
		}
	}

	return ret
}


func (user *User) FromMap(data map[string]string) {
	if name, ok := data[UserFieldName]; ok {
		user.Name = &name
	}

	if surname, ok := data[UserFieldSurname]; ok {
		user.Surname = &surname
	}

	if email, ok := data[UserFieldEmail]; ok {
		user.Email = &email
	}

	if value, ok := data[UserFieldAge]; ok {
		if age, err := strconv.ParseInt(value, 10, 32); err == nil {
			age_value := int(age)
			user.Age = &age_value
		}
	}

	if value, ok := data[UserFieldDuplicate]; ok {
		if duplicate, err := strconv.ParseBool(value); err == nil {
			user.Duplicate = &duplicate
		}
	}

	if addr_city, ok := data[UserFieldAddressCity]; ok {
		if user.Address == nil {
			addr := Address{}
			user.Address = &addr
		}
		user.Address.City = &addr_city
	}

	if addr_street, ok := data[UserFieldAddressStreet]; ok {
		if user.Address == nil {
			addr := Address{}
			user.Address = &addr
		}
		user.Address.Street = &addr_street
	}
}


// Clustered Index value in format <Name:Surname:Duplicate>
func (user *User) clusteredValue() (ret string, err error) {
	if user.Name != nil && user.Surname != nil {
		is_duplicate := false
		if user.Duplicate != nil { is_duplicate = *user.Duplicate }
		ret = fmt.Sprintf("%s:%s:%t", *user.Name, *user.Surname, is_duplicate)
	} else {
		err = fmt.Errorf("User Name and Surname must be set")
	}
	return
}


func (user *User) setNumIndex(
		tx *redis.Tx, field string, value float64) error {

	if user.Id == nil {
		return fmt.Errorf("User Id must be set")
	}

	tx.ZAdd(IndexUser + field, redis.Z{Score: value, Member: *user.Id})

	return nil
}


func (user *User) unsetNumIndex(tx *redis.Tx, field string) error {

	if user.Id == nil {
		return fmt.Errorf("User Id must be set")
	}

	tx.ZRem(IndexUser + field, *user.Id)

	return nil
}


func (user *User) setLexIndex(
		tx *redis.Tx, field string, value string) error {

	if user.Id == nil {
		return fmt.Errorf("User Id must be set")
	}

	user_id := fmt.Sprint(*user.Id)

	ikey := IndexUser + field
	ckey := IndexContentUser + field

	if res, _ := tx.HGet(ckey, user_id).Result(); res != "" {
		tx.ZRem(ikey, res)
	}

	lex_value := value + ":" + user_id
	tx.HSet(ckey, user_id, lex_value)
	tx.ZAdd(ikey, redis.Z{Score: 0, Member: lex_value})

	return nil
}


func (user *User) unsetLexIndex(tx *redis.Tx, field string) error {

	if user.Id == nil {
		return fmt.Errorf("User Id must be set")
	}

	user_id := fmt.Sprint(*user.Id)

	ikey := IndexUser + field
	ckey := IndexContentUser + field

	if res, err := tx.HGet(ckey, user_id).Result(); err == nil {
		tx.ZRem(ikey, res)
	} else {
		return err
	}

	tx.HDel(ckey, user_id)

	return nil
}


func (user *User) setIndexes(tx *redis.Tx, fields FieldMap) error {
	if value, err := user.clusteredValue(); err == nil {
		user.setLexIndex(tx, IndexUserClustered, value)
	}

	for field, value := range fields {
		switch value.(type) {
		case int:
			user.setNumIndex(tx, field, float64(value.(int)))
		case bool:
			user.setLexIndex(tx, field, strconv.FormatBool(value.(bool)))
		case string:
			user.setLexIndex(tx, field, value.(string))
		default:
			return fmt.Errorf("Unknown value type for Index: '%T'", value)
		}
	}

	return nil
}


func (user *User) removeIndexes(tx *redis.Tx, fields FieldMap) error {
		user.unsetLexIndex(tx, IndexUserClustered)

		for field, value := range fields {
			switch value.(type) {
			case int:
				user.unsetNumIndex(tx, field)
			case bool:
				user.unsetLexIndex(tx, field)
			case string:
				user.unsetLexIndex(tx, field)
			default:
				return fmt.Errorf("Unknown value type for Index: '%T'", value)
			}
		}
		return nil
}


func (user *User) Create(db *redis.Client) error {
	if user.Id != nil {
		return fmt.Errorf("User Id must not be set")
	} else if user.Name == nil || *user.Name == "" {
		return fmt.Errorf("User Name must be set")
	} else if user.Surname == nil || *user.Surname == "" {
		return fmt.Errorf("User Surname must be set")
	}

	fields := user.ToFieldMap()
	if _, ok := fields[UserFieldDuplicate]; !ok {
		fields[UserFieldDuplicate] = false
	}

	err := db.Watch(func (tx *redis.Tx) error {

		if clustered_value, err := user.clusteredValue(); err == nil {
			min := fmt.Sprintf("[%s:", clustered_value)
			max := min + `\ff`

			list, err := db.ZRangeByLex(
					IndexUser + IndexUserClustered,
					redis.ZRangeBy{Min: min, Max: max}).Result()

			if err != nil {
				return err
			} else if len(list) > 0 {
				return fmt.Errorf(
					"User with key '%s' already exists", clustered_value)
			}

		} else {
			return err
		}

		user_id := tx.Incr(IdKeyUser).Val()

		key := makeKey(UserClassStr, user_id)
		if err := tx.HMSet(key, fields).Err(); err != nil {
			return err
		}

		user.Id = &user_id
		if err := user.setIndexes(tx, fields); err != nil {
			return err;
		}

		return nil
	}, IdKeyUser)

	return err
}


func (user *User) Update(db *redis.Client) error {
	if user.Id == nil {
		return fmt.Errorf("User Id must be set")
	}

	fields := user.ToFieldMap()
	if len(fields) == 0 {
		return fmt.Errorf("Nothing to update")
	}

	key := makeKey(UserClassStr, *user.Id)
	err := db.Watch(func (tx *redis.Tx) error {
		if flag, err := tx.Exists(key).Result(); err != nil || flag != 1 {
			if err != nil { return err }
			return fmt.Errorf("No such User ID '%d'", *user.Id)
		}

		if err := tx.HMSet(key, fields).Err(); err == nil {
			if str_map, err := tx.HGetAll(key).Result(); err == nil {
				user.FromMap(str_map)
				fields = user.ToFieldMap()
			} else { return err }
		} else { return err }

		if err := user.setIndexes(tx, fields); err != nil {
			return err;
		}

		return nil
	}, key)

	return err
}


func (user *User) Delete(db *redis.Client) error {
	if user.Id == nil {
		return fmt.Errorf("User Id must be set")
	}

	key := makeKey(UserClassStr, *user.Id)
	err := db.Watch(func (tx *redis.Tx) error {
		if flag, err := tx.Exists(key).Result(); err == nil {
			if flag == 0 {
				return fmt.Errorf("No such User ID '%d'", *user.Id)
			}
		} else { return err }

		if str_map, err := tx.HGetAll(key).Result(); err == nil {
			user.FromMap(str_map)
		} else { return err }

		fields := user.ToFieldMap()

		for field, _ := range fields {
			if err := tx.HDel(key, field).Err(); err != nil {
				return err
			}
		}

		return user.removeIndexes(tx, fields)
	}, key)

	return err
}


func (user *User) Get(db *redis.Client) error {
	if user.Id == nil {
		return fmt.Errorf("User Id must be set")
	}

	key := makeKey(UserClassStr, *user.Id)
	err := db.Watch(func (tx *redis.Tx) error {
		if flag, err := tx.Exists(key).Result(); err == nil {
			if flag == 0 {
				return fmt.Errorf("No such User ID '%d'", *user.Id)
			}
		} else { return err }

		if str_map, err := tx.HGetAll(key).Result(); err == nil {
			user.FromMap(str_map)
		} else { return err }

		return nil
	}, key)

	return err
}


func (user *User) Find(db *redis.Client) (ret []User, err error) {
	var list []string

	if value, err := user.clusteredValue(); err == nil {
		// Find by Clustered index
		min := fmt.Sprintf("[%s:", value)
		max := min + `\ff`
		list, err = db.ZRangeByLex(
			IndexUser + IndexUserClustered,
			redis.ZRangeBy{Min: min, Max: max}).Result()
	} else {
		fields := user.ToFieldMap()
		if len(fields) > 0 {
			// Find by first setted field
			for field, value := range fields {
				key := IndexUser + field
				switch value.(type) {
				case int:
					min_max := fmt.Sprint(value)
					list, err = db.ZRangeByScore(
						key,
						redis.ZRangeBy{Min: min_max, Max: min_max}).Result()
				case string:
					min := fmt.Sprintf("[%s:", value)
					max := min + `\ff`
					list, err = db.ZRangeByLex(
						key,
						redis.ZRangeBy{Min: min, Max: max}).Result()
				case bool:
					min := fmt.Sprintf("[%t:", value.(bool))
					max := min + `\ff`
					list, err = db.ZRangeByLex(
						key,
						redis.ZRangeBy{Min: min, Max: max}).Result()
					default:
						continue
				}
				break
			}
		} else {
			// return all
			list, err = db.Keys("User:*").Result()
		}
	}

	if err != nil { return }

	for _, user_id_str := range list {
		if ind := strings.LastIndex(user_id_str, ":"); ind != -1 {
			user_id_str = user_id_str[ind+1:]
		}
		if user_id, err := strconv.ParseInt(user_id_str, 10, 64); err == nil {
			user := User{Id: &user_id}
			if err := user.Get(db); err == nil {
				ret = append(ret, user)
			}
		}
	}

	return
}
