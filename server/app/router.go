package app

import (
	"net/http"
	"github.com/gorilla/mux"
	"github.com/go-redis/redis"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/nshibalov/golang-test-rest/server/app/endpoints"
)


func hrc_wrapper(
		counter *prometheus.CounterVec) func (http.HandlerFunc) http.HandlerFunc {
	return func (handle http.HandlerFunc) http.HandlerFunc {
		return promhttp.InstrumentHandlerCounter(counter, handle)
	}
}


func NewRouter(db *redis.Client, metrics *Metrics) *mux.Router {
	wrapper := hrc_wrapper(metrics.HttpRequestTotal)
	router := mux.NewRouter()
	router.HandleFunc(
		"/users", wrapper(endpoints.FindUser(db))).Methods("GET")
	router.HandleFunc(
		"/users", wrapper(endpoints.CreateUser(db))).Methods("POST")
	router.HandleFunc(
		"/users", wrapper(endpoints.UpdateUser(db))).Methods("PUT")
	router.HandleFunc(
		"/users", wrapper(endpoints.DeleteUser(db))).Methods("DELETE")
	router.HandleFunc(
		"/users/{id}", wrapper(endpoints.GetUser(db))).Methods("GET")
	router.Handle(
		"/metrics", promhttp.HandlerFor(
			metrics.Registry, promhttp.HandlerOpts{})).Methods("GET")
	return router
}
