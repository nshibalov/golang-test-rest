package app

import (
	"github.com/prometheus/client_golang/prometheus"
)


type Metrics struct {
	Registry *prometheus.Registry

	Version prometheus.Gauge
	HttpRequestTotal *prometheus.CounterVec
}


func (metrics *Metrics) Init() {
	metrics.Version = prometheus.NewGauge(prometheus.GaugeOpts{
			Name: "USER_API_VERSION",
			Help: "Test GoLang REST API",
			ConstLabels: map[string]string{
				"version": "v0.0.1",
			},
		})

	metrics.HttpRequestTotal = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "HTTP_REQUESTS_TOTAL",
			Help: "Count of all HTTP requests",
		},
		[]string{"code", "method"},
	)

	metrics.Registry = prometheus.NewRegistry()
	metrics.Registry.MustRegister(metrics.Version)
	metrics.Registry.MustRegister(metrics.HttpRequestTotal)
}
