package app

import (
	"os"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/go-redis/redis"
)


type Core struct {
	Port string
	DB *redis.Client
	Router *mux.Router
	Metrics Metrics
}


func (core *Core) initDB() {
	core.DB = redis.NewClient(&redis.Options{
		Addr: "redis:6379",
		Password: "", // no password set
		DB: 0,  // use default DB
	})

	pong, err := core.DB.Ping().Result()
	if err != nil {
		log.Println("Error initializing DB:", err)
	} else {
		log.Println("DB:", pong)
	}
}


func (core *Core) Init() {
	log.Println("Core.Init")

	core.Port = os.Getenv("PORT")
	if len(core.Port) == 0 {
		core.Port = "3000"
	}


	core.initDB()
	core.Metrics.Init()
	core.Router = NewRouter(core.DB, &core.Metrics)
}


func (core *Core) Finalize() {
	log.Println("Core.Finalize")
}


func (core *Core) Run() {
	log.Printf("Serving at port: %s", core.Port)
	log.Fatal(http.ListenAndServe(":" + core.Port, core.Router))
}


func NewCore() *Core {
	core := &Core{}
	core.Init()
	return core
}
