package main

import "gitlab.com/nshibalov/golang-test-rest/server/app"


func main() {
	core := app.NewCore()
	defer core.Finalize()

	core.Run()
}
