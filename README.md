# User API

Простой User CRUD REST API, написан на GoLang + REDIS + Prometheus
Основа клиента была сгенерена с помощью Swagger

## Сервер

### Статистика
 - Количество запросов HTTP
 - Версия API
 
### Ограничения
 1. Поиск возможен только по одному параметру.
 2. Поиск только по точному соответствию.


## Клиент
 - Принимает параметры для поиска в виде JSON.
 - Ищет записи по в БД.
 - Создает копии записей через сервер с **"Duplicate": true**.
 - Если сервер не доступен, осуществляет операцию напрямую через БД.

## Использование

### Подготовка
```sh
git clone https://gitlab.com/nshibalov/golang-test-rest.git
cd golang-test-rest
docker-compose up
```

После этого поднимутся 4 контейнера:
 - REDIS
 - SERVER (REST API) [http://localhost:3000]
 - CLIENT (В режиме сервиса) [http://localhost:3030]
 - METRICS (Prometheus) [http://localhost:9090]

### Клиент
```sh
cd client
go build && ./client -search '{}'
```
```
Usage of ./client:
  -search string
    	a search string (default "{\"Duplicate\": false}")
  -service
    	run as service
```

## Примеры curl-запросов

### Создание
```sh
curl -X POST "localhost:3000/users" -H  "accept: application/json" -H  "Content-Type: application/json" -d '{"name": "Ivan", "surname": "Pertrov", "age": 34, "email": "ipetro1@test.ru", "address": {"city": "Moscow"}}'
```

### Обновление
```sh
curl -X PUT "localhost:3000/users" -H  "accept: application/json" -H  "Content-Type: application/json" -d '{"id":1, "surname":"Ivanov", "age":35}'
```

### Удаление
```sh
curl -X DELETE "localhost:3000/users" -H  "accept: application/json" -H  "Content-Type: application/json" -d '{"id":1}'
```

### Поиск
```sh
curl -X GET "localhost:3000/users" -H  "accept: application/json" -H  "Content-Type: application/json" -d '{"age":35}'
curl -X GET "localhost:3000/users" -H  "accept: application/json" -H  "Content-Type: application/json" -d '{"Address":{"City": "Moscow"}}'
```

### Просмотр
```sh
curl -X GET "localhost:3000/users/1" -H  "accept: application/json"
```

### Дублирование (Клиент)
```sh
curl -X GET "localhost:3030/" -d '{"age": 34}'
